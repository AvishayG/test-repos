<?php
/*
Plugin Name: test repos
Version: 1.3
*/

require 'plugin-update-checker-4.5.1/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/AvishayG/test-repos',
	__FILE__,
	'test-repos'
);


function foo(){
	$baz = 'bar';

	echo 'foo!!';
}
add_action('plugins_loaded', 'foo');